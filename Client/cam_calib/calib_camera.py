#!/usr/bin/env python

"""
From https://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_calib3d/py_calibration/py_calibration.html#calibration
Calling:
cameracalib.py  <folder> <image type> <num rows> <num cols> <cell dimension>
like cameracalib.py folder_name png
--h for help
"""
__author__ = "Tiziano Fiorenzani"
__date__ = "01/06/2018"

import numpy as np
import cv2
import glob
import sys
import argparse
import time

# --- SET PARAMETERS ---
nRows = 7
nCols = 9
dimension = 0.02 # size of square side in m

workingFolder   = "."
img_dir = "./img"
imageType       = 'png'

# img_to_undistort is undistorted and saved as calibresult.png; additionally the 
# new, scaled camera matrix is saved in cameraMatrix.txt
img_to_undistort = "./img/0002.png"

# --- SET OPTIONS ---
crop_image = True # crop undistorted image to roi without invisible areas

# termination criteria
max_number_of_iterations = 100
calib_eps = 0.001
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, max_number_of_iterations, calib_eps)

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((nRows*nCols,3), np.float32)
objp[:,:2] = np.mgrid[0:nCols,0:nRows].T.reshape(-1,2)*dimension

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

if '-h' in sys.argv or '--h' in sys.argv:
    print("\n IMAGE CALIBRATION GIVEN A SET OF IMAGES")
    print(" call: python cameracalib.py <folder> <image type> <num rows (9)> <num cols (6)> <cell dimension (25)>")
    print("\n The script will look for every image in the provided folder and will show the pattern found." \
          " User can skip the image pressing ESC or accepting the image with RETURN. " \
          " At the end the end the following files are created:" \
          "  - cameraDistortion.txt" \
          "  - cameraMatrix.txt \n\n")

    sys.exit()

# Find the images files
filename    = img_dir + "/*." + imageType
images      = glob.glob(filename)

if len(images) < 9:
    print("Not enough images were found: at least 9 shall be provided!!!")
    sys.exit()
else:
    nPatternFound = 0
    idx_0002 = None     # var to store processing index of image '0002.png'

    for fname in images:
        if 'calibresult' in fname: 
            continue

        #-- Read the file and convert in greyscale
        img     = cv2.imread(fname)
        gray    = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

        print("Reading image ", fname)

        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray, (nCols,nRows))

        # If found, add object points, image points (after refining them)
        if ("0003" not in fname and ret == True) or (ret == False and "0002" in fname):
            print("Pattern found! Press ESC to skip or ENTER to accept")

            # load coordinates of manually defined image positions for 0002.png and 0003.png
            if "0002" in fname:
                ret = True
                corners2 = np.genfromtxt(workingFolder + "/coords/0002.txt", dtype=np.float32)
                corners2 = corners2.reshape((63,1,2))
            elif "0001" in fname:
                corners2 = np.genfromtxt(workingFolder + "/coords/0001.txt", dtype=np.float32)
                corners2 = corners2.reshape((63,1,2))
                ret = True
            else:
                #--- Sometimes, Harris Corner fails with crappy pictures, so
               corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)

            # Draw and display the corners
            cv2.drawChessboardCorners(img, (nCols,nRows), corners2,ret)
            
            if "0002" in fname:
                # store index of image 0002.png for later saving of rvecs and tvecs
                idx_0002 = len(objpoints)
            
            print("Image accepted")
            nPatternFound += 1
            objpoints.append(objp)
            imgpoints.append(corners2)
        else:
            print("Image Skipped")


if (nPatternFound > 1):
    print("Found %d good images" % (nPatternFound))
    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)

    # Undistort an image
    img = cv2.imread(img_to_undistort)
    h,  w = img.shape[:2]
    print("Image to undistort: ", img_to_undistort)
    newcameramtx, roi =cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),1,(w,h))

    # undistort
    time1 = time.time()
    mapx,mapy = cv2.initUndistortRectifyMap(mtx,dist,None,newcameramtx,(w,h), cv2.CV_32FC1)
    mapx, mapy = cv2.convertMaps(mapx, mapy, cv2.CV_16SC2)
    time_sum = 0
    for i in range(100):
        time1 = time.time()
        dst = cv2.remap(img,mapx,mapy, cv2.INTER_LINEAR)
        time2 = time.time()
        time_sum += (time2-time1)
    print("Int remapping:", (time_sum/100))
    time1 = time.time()
    dst = cv2.undistort(img, mtx, dist, None, newcameramtx)
    #print("Single call:", str(time.time()-time1))

    # crop the image
    if crop_image:
        x,y,w,h = roi
        dst = dst[y:y+h, x:x+w]
        print("ROI: ", x, y, w, h)

    cv2.imwrite(workingFolder + "/calibresult.png",dst)
    print("Calibrated picture saved as calibresult.png")
    print("Calibration Matrix: ")
    print(mtx)
    print("New Calibration Matrix: ")
    print(newcameramtx)
    print("Disortion: ", dist)

    #--------- Save result
    filename = workingFolder + "/params/scaled_intrinsic_matrix.csv"
    np.savetxt(filename, newcameramtx, delimiter=",")
    filename = workingFolder + "/params/intrinsic_matrix.csv"
    np.savetxt(filename, mtx, delimiter=",")
    filename = workingFolder + "/params/camera_distortion.csv"
    np.savetxt(filename, dist, delimiter=",")
    filename = workingFolder + "/params/roi.csv"
    np.savetxt(filename, roi, delimiter=",")
    print("MAPX TYPE", mapx.dtype)
    print("MAPY TYPE", mapy.dtype)
    mapx_reshaped = np.reshape(mapx, (mapx.shape[0]*mapx.shape[2], mapx.shape[1]))
    filename = workingFolder + "/params/mapx.csv"
    np.savetxt(filename, mapx_reshaped, delimiter=",")
    filename = workingFolder + "/params/mapy.csv"
    np.savetxt(filename, mapy, delimiter=",")

    # save 4x4 extrinsic matrix for x-y-plane
    if idx_0002 is not None:
        # image 0002.png was used in calibration
        translation_homogenous = np.vstack((tvecs[idx_0002], np.array([[1]])))
        rotation_matrix33, _ = cv2.Rodrigues(rvecs[idx_0002])
        rotation_matrix34 = np.vstack((rotation_matrix33, np.array([[0,0,0]])))
        extrinsic_matrix44 = np.hstack((rotation_matrix34, translation_homogenous))
        filename = workingFolder + "/params/extrinsic_matrix.csv"
        np.savetxt(filename, extrinsic_matrix44, delimiter=",")

    sum_of_error = 0
    errors = list()
    for i in range(len(objpoints)):
        imgpoints2, _ = cv2.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
        error = cv2.norm(imgpoints[i],imgpoints2, cv2.NORM_L2)/len(imgpoints2)
        sum_of_error += abs(error)
        errors.append(abs(error))

    print("total error: ", sum_of_error/len(objpoints))
    print("maximal error: ", max(errors))

else:
    print("In order to calibrate you need at least 9 good pictures... try again")
