#!/usr/bin/env python3

"""Implements CarController for Jetson."""

from typing import Any, Dict, Tuple

import adafruit_motor.servo
from adafruit_pca9685 import PCA9685
from adafruit_servokit import ServoKit
from board import SCL, SDA
import busio
import numpy as np
from numpy_ringbuffer import RingBuffer

from process_wrapper import ProcessWrapper
from control.plan_trajectory import compute_lane_based_trajectory_vector, line_to_trajectory_components, create_weight_mat, compute_lwma, MAX_TRAJECTORY_LENGTH_PX, proximity_alert
from sensors.camera_transformation import CameraTransformer
from image_processing.join_lines import MIN_VERTICAL_SLOPE


class CarController(ProcessWrapper):
    """Class for car control, which is executed in a separate process."""
    
    def __init__(self, shared_control_speed_data_ptr: Any, shared_control_steering_data_ptr: Any, 
                 shared_manual_control_data_ptr: Any, shared_lane_data_ptr: Any, 
                 shared_object_data_ptr: Any, shared_object_certainty_data_ptr: Any, shared_ultrasonic_data_ptr: Any,
                 control_application_event: Any, data_copy_event: Any, data_sending_event: Any, 
                 image_capture_event: Any, stop_event: Any, emergency_stop_event: Any, startup_barrier: Any, 
                 config: Dict, camera_transformer: CameraTransformer, max_detectable_lanes: int, max_detectable_obj: int, neutral_speed: int, neutral_steering: int):
        """
        Initializes CarController.

        Args:
            shared_control_speed_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for speed value
            shared_control_steering_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for steering
            value
            shared_manual_control_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for manual 
            control flag
            shared_lane_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            lane data
            shared_object_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            object data
            shared_object_certainty_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_float]): Shared memory 
            pointer for object distances
            shared_ultrasonic_data_ptr (multiprocessing.Array[ctypes.c_float]): Shared memory pointer 
            for ultrasonic data
            control_application_event (multiprocessing.Event): Event for control application initiation
            data_copy_event (multiprocessing.Event): Event for copying data
            data_sending_event (multiprocessing.Event): Event for data transmission initiation
            image_capture_event (multiprocessing.Event): Event for image capture initiation
            emergency_stop_event (multiprocessing.Event): Event for emergency stop signal
            startup_barrier (multiprocessing.Barrier): Barrier that synchronizes startup of all processes
            config (Dict): Software configuration
            camera_transformer (CameraTransformer): object containing camera parameters
            max_detectable_lanes (int): Maximum number of lanes that can be detected at a time
            max_detectable_obj (int): Maximum number of objects that can be detected at a time
            neutral_speed (int): Neutral value for speed parameter
            neutral_steering (int): Neutral value for steering parameter
        """
        
        self.config = config
        self.max_detectable_lanes = max_detectable_lanes
        self.max_detectable_obj = max_detectable_obj
        self.neutral_speed = neutral_speed
        self.neutral_steering = neutral_steering
        self.camera_transformer = camera_transformer

        # init events and barriers
        self.control_application_event = control_application_event
        self.data_copy_event = data_copy_event
        self.data_sending_event = data_sending_event
        self.image_capture_event = image_capture_event
        self.stop_event = stop_event
        self.emergency_stop_event = emergency_stop_event
        self.startup_barrier = startup_barrier
        
        # create deque for trajectory values
        self._trajectory_ringbuffer = RingBuffer(self.config["HISTORY_LENGTH"], dtype=(np.float32, 2))
        self._weight_matrix = create_weight_mat(self.config["HISTORY_LENGTH"])
        
        # init process
        shared_memory_pointers = (shared_control_speed_data_ptr, shared_control_steering_data_ptr, 
                                  shared_manual_control_data_ptr, shared_lane_data_ptr, shared_object_data_ptr, 
                                  shared_object_certainty_data_ptr, shared_ultrasonic_data_ptr)
        super().__init__(shared_memory_pointers, emergency_stop_event)
        
    def _init(self):
        """Initializes CarController process specifically."""
        
        # init connection to servo control board
        # open up i2c_bus
        self.logger.info("Opening I2C bus...")
        self.i2c_bus = busio.I2C(SCL, SDA)
        self.logger.info("I2C bus has been opened.")
        self.logger.info("Initializing PCA9685 servo driver...")
        self.pca = PCA9685(self.i2c_bus)
        # determine servo kit channels
        self.kit = ServoKit(channels=16)
        # set pca frequency
        self.pca.frequency = 50
        # set up engine and steering
        self.engine = adafruit_motor.servo.Servo(self.pca.channels[0])
        self.steering = adafruit_motor.servo.Servo(self.pca.channels[1])
        self.logger.info("PCA9685 servo driver has been initialized.")
        self.enginge_stop = False # flag to be set by proximity alert

        # set neutral values while powering up
        self.logger.info("Setting neutral values for engine and steering parameters...")
        self.engine.angle = self.neutral_speed
        self.steering.angle = self.neutral_steering
        self.logger.info("Neutral values for engine (%d) and steering (%d) have been set.", self.neutral_speed, 
                         self.neutral_steering)

        # set boundaries
        self._max_speed_back = self.config['ENGINE']['MAX_SPEED_BACK']
        self._max_speed_front = self.config['ENGINE']['MAX_SPEED_FRONT']
        self._max_left = self.config['STEER']['MAX_LEFT']
        self._max_right = self.config['STEER']['MAX_RIGHT']
        self._max_angle_deviation = self.config['STEER']['MAX_ANGLE_DEVIATION']
        
        # parameters
        # minimal speed level of car
        self.BASE_SPEED = 90
        # maximal difference of last steering angle and 90 degrees (neutral steering) to reduce speed 
        self.ANGLE_DIF_TO_REDUCE_SPEED = 10
        # angle difference between last steering angle and 90 degrees (neutral) is divided by this value -> speed is reduced by the result
        self.STEERING_DIF_DIVIDER = 10 
        # value the normalized trajectory length is multiplicated with to calculated the actual speed
        self.SPEED_MULTIPLICATOR = 11
        # value the "transformed" trajectory vector slope is multiplicated with to calculate the steering (for steering to the right)
        self.STEER_RIGHT_MULTIPLICATOR = 85
        # value the "transformed" trajectory vector slope is multiplicated with to calculate the steering (for steering to the left)
        self.STEER_LEFT_MULTIPLICATOR = 80

    def _plan_trajectory(self, current_speed: int, current_steering: int, shared_lane_data: np.ndarray, 
                         shared_object_data: np.ndarray, shared_distance_data: np.ndarray, 
                         shared_ultrasonic_data: np.ndarray) -> Tuple[int, int]:
        """
        Plans the trajectory, based on current driving state and (processed) sensor data.

        Args:
            current_speed (int): Current speed parameter
            current_steering (int): Current steering parameter
            shared_lane_data (np.ndarray[n, 4][np.uint16]): Lane data as [[xl0, yl0, xl1, yl1], [xr0, yr0, xr1, yr1]] 
            array
            shared_object_data (np.ndarray[n, 5][np.uint16]): Detected object data as 
            [[obj_id, x0, x1, y0, y1]*max_obj_count] array
            shared_distance_data (np.ndarray[n][np.float32]): Object distances as [dist]*max_obj_count array
            shared_ultrasonic_data (np.ndarray[3][np.float32]): Ultrasonic measurements as [REAR, FRONT, RIGHT] array

        Returns:
            int: Adapted speed parameter
            int: Adapted steering parameter
        """
        lane_based_trajectory =  compute_lane_based_trajectory_vector(
            shared_lane_data, 
            self.camera_transformer.inv_intrinsic_cam_matrix_33, 
            self.camera_transformer.inv_extrinsic_cam_matrix_44, 
            self.camera_transformer.initial_size[0], 
            int(self.camera_transformer.roi[1] + self.camera_transformer.roi[3]//3))
        lane_trajectory_components = line_to_trajectory_components(lane_based_trajectory)
        # TODO: compute trafic sign based trajectory
        traffic_sign_trajectory_components = np.array([0,0], dtype=np.float32)

        self.enginge_stop = proximity_alert(shared_ultrasonic_data, current_speed, self.neutral_speed)
        # TODO: fuse trajectory values from lane based, ultrasonic signal based and traffic sign based
        fused_trajectory = self._fuse_trajectory_components(lane_trajectory_components, traffic_sign_trajectory_components)
        # TODO: check if its more convenient to map trajectory components separately 
        fused_trajectory[0] = CarController._map_slope(fused_trajectory[0])
        current_trajectory_angles = self._map_trajectory_components_to_angles(fused_trajectory)
        self._trajectory_ringbuffer.append(current_trajectory_angles)
        new_speed, new_steering = self._calc_avg_trajectory_angles()
        return int(new_speed), int(new_steering)

    def _run(self, shared_control_speed_data_ptr: Any, shared_control_steering_data_ptr: Any, 
             shared_manual_control_data_ptr: Any, shared_lane_data_ptr: Any, shared_object_data_ptr: Any, 
             shared_object_certainty_data_ptr: Any, shared_ultrasonic_data_ptr: Any):
        """
        Cyclic worker method, sets speed and steering values.

        Args:
            shared_control_speed_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for speed value
            shared_control_steering_data_ptr (Vmultiprocessing.alue[ctypes.c_uint8]): Shared memory pointer for steering
            value
            shared_manual_control_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for manual 
            control flag
            shared_lane_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            lane data
            shared_object_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            object data
            shared_object_certainty_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_float]): Shared memory 
            pointer for object distances
            shared_ultrasonic_data_ptr (multiprocessing.Array[ctypes.c_float]): Shared memory pointer 
            for ultrasonic data
        """
        
        current_speed = self.neutral_speed
        current_steering = self.neutral_steering
        
        # create numpy arrays, representing views onto shared memory
        shared_lane_data = np.frombuffer(shared_lane_data_ptr, dtype=np.uint16).reshape((self.max_detectable_lanes, 4))
        shared_object_data = np.frombuffer(shared_object_data_ptr, dtype=np.uint16).reshape((self.max_detectable_obj, 
                                                                                             5))
        shared_distance_data = np.frombuffer(shared_object_certainty_data_ptr, dtype=np.float32)
        shared_ultrasonic_data = np.frombuffer(shared_ultrasonic_data_ptr.get_obj(), dtype=np.float32)

        # wait until all processes are ready on startup
        self.logger.debug("Waiting for Startup...")
        self.startup_barrier.wait()

        while True:
            # wait until data has been copied (in case of autonomous control) or data has been received (in case of
            # manual control)
            self.logger.debug("Waiting for E_3...")
            self.control_application_event.wait()
            self.logger.debug("E_3 set. Changing car controls...")
            
            # wait, if stop signal was given by remote
            if not self.stop_event.is_set():
                self.logger.info("Stopped until remote gives signal to proceed.")
            self.stop_event.wait() 
            
            # if manual control is activated, just fetch speed and steering parameters from shared memory
            if shared_manual_control_data_ptr.value:
                self.logger.debug("Manual control enabled.")
                current_speed = shared_control_speed_data_ptr.value
                current_steering = shared_control_steering_data_ptr.value
            # otherwise, plan the trajectory and update shared memory
            else:
                current_speed, current_steering = self._plan_trajectory(current_speed, current_steering, 
                                                                        shared_lane_data, shared_object_data, 
                                                                        shared_distance_data, shared_ultrasonic_data)
                shared_control_speed_data_ptr.value = current_speed
                shared_control_steering_data_ptr.value = current_steering

            # check speed boundaries and clip, if necessary
            if current_speed < self._max_speed_back or current_speed > self._max_speed_front:
                self.logger.warn("Current speed (%d) is out of the allowed range [%d, %d] and will be clipped." % 
                                 (current_speed, self._max_speed_back, self._max_speed_front))
            current_speed = max(self._max_speed_back, min(self._max_speed_front, current_speed))

            # check steering boundaries and clip, if necessary
            if current_steering < self._max_left or current_speed > self._max_right:
                self.logger.warn("Current steering angle (%d) is out of the allowed range [%d, %d] and will be clipped."
                                 % (current_steering, self._max_left, self._max_right))
            current_steering = max(self._max_left, min(self._max_right, current_steering))
            
            if abs(self.steering.angle - current_steering) >= 1:
                self.steering.angle = current_steering
                
            # set engine servo angle
            if self.enginge_stop:
                self.engine.angle = self.neutral_speed
            elif abs(self.engine.angle - current_speed) >= 1:
                self.engine.angle = current_speed
            
            # TODO: Stabilizer for steering (use _max_angle_deviation)
            # https://towardsdatascience.com/deeppicar-part-4-lane-following-via-opencv-737dd9e47c96

            # reset waiting event
            self.control_application_event.clear()
            self.logger.debug("Car controls changed. E_3 cleared.")
            
            # check if no manual control 
            if not shared_manual_control_data_ptr.value:
                # if transmitter process is busy -> notify camera process
                if self.data_sending_event.is_set():
                    self.logger.debug("Transmitter busy. Notifying Camera...")
                    self.image_capture_event.set()
                    self.logger.debug("Set E_1.")
                # if transmitter process is ready -> notify copy worker process
                else:
                    self.logger.debug("Transmitter ready. Notifying Copy Worker...")
                    self.data_copy_event.set()
                    self.logger.debug("Set E_4.")
            
    def _on_termination(self) -> None:
        """Is invoked on process termination. Resets neutral engine and steering values."""
        # check if engine and steering resources are opened
        if self.engine is not None and self.steering is not None:
            self.logger.info("Re-setting neutral values for engine and steering parameters...")
            self.engine.angle = self.neutral_speed
            self.steering.angle = self.neutral_steering
            self.logger.info("Neutral values for engine (%d) and steering (%d) have been reset.", self.neutral_speed, 
                         self.neutral_steering)

    def _fuse_trajectory_components(self, lane_based_trajectory_components: np.ndarray, 
                                    traffic_sign_based_trajectory_components: np.ndarray) -> np.ndarray:
        """Fuse lane based, traffic sign based and ultrasonic based trajectory components. 

        Args:
            lane_based_trajectory_components (np.ndarray):  trajectory components calculated by lane based trajectory 
                                                            (normalized length and slope)
            traffic_sign_based_trajectory_components (np.ndarray): trajectory components calculated by traffic sign based trajectory

        Returns:
            np.ndarray: fused trajectory components
        """
        # TODO: implement fusing
        fused_trajectory = lane_based_trajectory_components
        # TODO: check return is np.float32
        return fused_trajectory
    
    def _calc_avg_trajectory_angles(self) -> np.ndarray:
        """Calculate average trajectory angles by last ten values, which are weighted linear. Additionally the speed is
        reduced, if the steering to the right or left side is bigger than CarController.ANGLE_DIF_TO_REDUCE_SPEED 
        degrees.

        Returns:
            np.ndarray: averaged speed and steering angles as two element numpy array 
        """
        trajectory_matrix = np.array(self._trajectory_ringbuffer)
        avg_angles = compute_lwma(trajectory_matrix, self._weight_matrix)
        steering_angle_diff = abs(trajectory_matrix[-1, 1] - self.config["STEER"]["NEUTRAL_STEERING"])

        if steering_angle_diff > self.ANGLE_DIF_TO_REDUCE_SPEED:
            avg_angles[0] -= steering_angle_diff / self.STEERING_DIF_DIVIDER

        return avg_angles
    
    def _map_trajectory_components_to_angles(self, trajectory_components: np.ndarray) -> np.ndarray:
        """Map the trajectory components (length of trajectory vector and "transformed" slope of trajectory vector) to
        the actual speed and steering angles.

        Args:
            trajectory_components (np.ndarray): two element array containing length of trajectory vector and "transformed" slope of trajectory vector

        Returns:
            np.ndarray: two element array of speed angle and steering angle
        """
        new_speed = trajectory_components[1] / MAX_TRAJECTORY_LENGTH_PX * self.SPEED_MULTIPLICATOR + self.BASE_SPEED
        if trajectory_components[1] == 0 and len(self._trajectory_ringbuffer):
            # avoid change in steering if car stands
            new_steering = self._trajectory_ringbuffer.pop()[0]
        elif trajectory_components[0] < 0:
            # calculate steering angle for steering to the right
            new_steering = max(self._max_left, self.config["STEER"]["NEUTRAL_STEERING"] + self.STEER_RIGHT_MULTIPLICATOR * trajectory_components[0])
        else:
            # calculate steering angle for steering to the left
            new_steering = min(self._max_right, self.config["STEER"]["NEUTRAL_STEERING"] + self.STEER_LEFT_MULTIPLICATOR * trajectory_components[0])
        return np.array([new_speed, new_steering], dtype=np.float32)
    
    @staticmethod
    def _map_slope(slope: float) -> float:
        """Maps slope to values from -1 (slope = -MIN_VERTICAL_SLOPE) to 1 (slope = MIN_VERTICAL_SLOPE) - 
        slope = inf matches value 0

        Args:
            slope (float): slope to be mapped
            
        Returns:
            float: mapped slope value
        """
        if abs(slope) < 0.0001:
            # return 0, because slope of exactly 0 implicits that line is of length 0
            return 0
        else:
            return MIN_VERTICAL_SLOPE / slope
    