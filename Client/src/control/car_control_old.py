from adafruit_servokit import ServoKit
from board import SCL, SDA
import busio
from adafruit_pca9685 import PCA9685
import adafruit_motor.servo
import tty
import sys
import termios
import time

orig_settings = termios.tcgetattr(sys.stdin)
tty.setcbreak(sys.stdin)
x = 0

i2c_bus = busio.I2C(SCL, SDA)
pca = PCA9685(i2c_bus)

kit = ServoKit(channels=16)

pca.frequency = 50

motor = adafruit_motor.servo.Servo(pca.channels[0])
steer = adafruit_motor.servo.Servo(pca.channels[1])

motor.angle = 75  # neutral
steer.angle = 73
print("Ready to rumble!")
while x != chr(27):
    time.sleep(0.01)
    try:
        x = sys.stdin.read(1)[0]
        if x == 'w' and motor.angle < 100:
            motor.angle += 5
            print("motor: ", motor.angle)
        elif x == 's' and motor.angle > 50:
            motor.angle -= 5
            print("motor: ", motor.angle)
        elif x == 'a' and steer.angle > 3:
            steer.angle -= 2
            print("steer: ", steer.angle)
        elif x == 'd' and steer.angle < 170:
            steer.angle += 2
            print("steer: ", steer.angle)
        elif x == 'q':
            motor.angle = 75
            print("motor: ", motor.angle)

    except KeyboardInterrupt:
        motor.angle = 75
        steer.angle = 73
        break
termios.tcsetattr(sys.stdin, termios.TCSADRAIN, orig_settings)
