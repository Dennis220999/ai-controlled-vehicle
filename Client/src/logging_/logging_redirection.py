#!/usr/bin/env python3

"""Implements function to redirect uncaught exceptions to logging handlers."""

from logging import Logger
import traceback
from typing import Any, Callable


def redirect_exception(logger: Logger) -> Callable[[Any], None]:
    """
    Factory method, creating a function that redirects raised exception to logger.

    Args:
        logger (Logger): Logger of the corresponding process
    Returns:
        Callable[[Any], None]: Exception handling function that implements redirection
    """
    
    def handle_exception(*exc_info):
        """
        Redirects raised exception to logger and triggers emergency stop signal.

        Args:
            exc_info (Tuple): Exception data (type, value, traceback)
        """
        
        # log traceback
        tb = "\n".join(traceback.format_exception(*exc_info))
        logger.error(tb)
    
    return handle_exception