#!/usr/bin/env python3

"""Implements factory method to create process-specific logging capabilities."""

from datetime import date
import logging
import os


def create_logger(name: str, level: int=logging.INFO) -> logging.Logger:
    """
    Factory method to create process-specific logging capabilities.

    Args:
        name (str): Name of the logger, should identify the process.
        level (int, optional): Logging level. Defaults to logging.INFO.

    Returns:
        logging.Logger: Process-specific logger.
    """
    
    # define filename, based on the current date and the process name
    current_date = date.today().strftime("%d_%m_%Y")
    file_name = os.path.join(os.path.dirname(__file__), "../../logs", current_date + "_" + name + ".log")
    
    # create logger with both error and file stream as output
    logger = logging.getLogger(name)
    stream_formatter = logging.Formatter("%(asctime)s [%(levelname)s] [%(name)s]: %(message)s")
    file_formatter = logging.Formatter("%(asctime)s [%(levelname)s]: %(message)s")
    fileHandler = logging.FileHandler(file_name, mode='a')
    fileHandler.setFormatter(file_formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(stream_formatter)
    logger.setLevel(level)
    logger.handlers = [fileHandler, streamHandler]
    
    return logger
