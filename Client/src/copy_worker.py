#!/usr/bin/env python3

"""Implements CopyWorker for Jetson."""

import numpy as np
from typing import Any, Tuple

from process_wrapper import ProcessWrapper


class CopyWorker(ProcessWrapper):
    """Class for data copying, which is executed in a separate process."""

    def __init__(self, shared_image_data_ptr: Any, shared_lane_data_ptr: Any, shared_object_data_ptr: Any, 
                 shared_object_certainty_data_ptr: Any, shared_ultrasonic_data_ptr: Any, 
                 shared_image_data_cpy_ptr: Any, shared_lane_data_cpy_ptr: Any, shared_object_data_cpy_ptr: Any, 
                 shared_object_certainty_data_cpy_ptr: Any, shared_ultrasonic_data_cpy_ptr: Any, 
                 data_copy_event: Any, data_sending_event: Any, image_capture_event: Any, stop_event: Any, emergency_stop_event: Any, 
                 startup_barrier: Any, img_roi: Tuple[int, int, int, int], max_detectable_lanes: int, 
                 max_detectable_obj: int):
        """
        Initializes CopyWorker.

        Args:
            shared_image_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint8]): Shared memory pointer for 
            OpenCV image
            shared_lane_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            lane data
            shared_object_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            object data
            shared_object_certainty_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_float]): Shared memory 
            pointer for object distances
            shared_ultrasonic_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_float]): Shared memory pointer 
            for ultrasonic data
            shared_image_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint8]): Shared memory pointer for 
            OpenCV image copy
            shared_lane_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            lane data copy
            shared_object_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer 
            for object data copy
            shared_object_certainty_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_float]): Shared 
            memory pointer for object distances copy
            shared_ultrasonic_data_cpy_ptr (multiprocessing.Array[ctypes.c_float]): Shared memory 
            pointer for ultrasonic data copy
            data_copy_event (multiprocessing.Event): Event for copying data
            data_sending_event (multiprocessing.Event): Event for data transmission initiation
            image_capture_event (multiprocessing.Event): Event for image capture initiation
            stop_event (multiprocessing.Event): Event for stop signal
            emergency_stop_event (multiprocessing.Event): Event for emergency stop signal
            startup_barrier (multiprocessing.Barrier): Barrier that synchronizes startup of all processes
            img_roi (Tuple[int, int, int, int]): Shape of image's region of interest (x, y, w, h)
            max_detectable_lanes (int): Maximum number of lanes that can be detected at a time
            max_detectable_obj (int): Maximum number of objects that can be detected at a time
        """
        
        self.img_roi = img_roi
        self.max_detectable_lanes = max_detectable_lanes
        self.max_detectable_obj = max_detectable_obj

        # init events and barriers
        self.data_copy_event = data_copy_event
        self.data_sending_event = data_sending_event
        self.image_capture_event = image_capture_event
        self.stop_event = stop_event
        self.emergency_stop_event = emergency_stop_event
        self.startup_barrier = startup_barrier
        
        # init process
        shared_memory_pointers = (shared_image_data_ptr, shared_lane_data_ptr, shared_object_data_ptr, 
                                  shared_object_certainty_data_ptr, shared_ultrasonic_data_ptr, 
                                  shared_image_data_cpy_ptr, shared_lane_data_cpy_ptr, shared_object_data_cpy_ptr, 
                                  shared_object_certainty_data_cpy_ptr, shared_ultrasonic_data_cpy_ptr)
        super().__init__(shared_memory_pointers, emergency_stop_event)

    def _init(self):
        """Initializes CopyWorker process specifically."""
        
        pass

    def _run(self, shared_image_data_ptr: Any, shared_lane_data_ptr: Any, shared_object_data_ptr: Any, 
             shared_object_certainty_data_ptr: Any, shared_ultrasonic_data_ptr: Any, shared_image_data_cpy_ptr: Any, 
             shared_lane_data_cpy_ptr: Any, shared_object_data_cpy_ptr: Any, 
             shared_object_certainty_data_cpy_ptr: Any, shared_ultrasonic_data_cpy_ptr: Any):
        """
        Cyclic worker method, copies data inside shared memory.

        Args:
            shared_image_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint8]): Shared memory pointer for 
            OpenCV image
            shared_lane_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            lane data
            shared_object_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            object data
            shared_object_certainty_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_float]): Shared memory 
            pointer for object distances
            shared_ultrasonic_data_ptr (multiprocessing.Array[ctypes.c_float]): Shared memory pointer 
            for ultrasonic data
            shared_image_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint8]): Shared memory pointer for 
            OpenCV image copy
            shared_lane_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            lane data copy
            shared_object_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer 
            for object data copy
            shared_object_certainty_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_float]): Shared 
            memory pointer for object distances copy
            shared_ultrasonic_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_float]): Shared memory 
            pointer for ultrasonic data copy
        """

        # create numpy arrays, representing views onto shared memory
        shared_img_data = np.frombuffer(shared_image_data_ptr, dtype=np.uint8).reshape(self.img_roi[3], self.img_roi[2], 
                                                                                       3)
        shared_lane_data = np.frombuffer(shared_lane_data_ptr, dtype=np.uint16).reshape((self.max_detectable_lanes, 4))
        shared_object_data = np.frombuffer(shared_object_data_ptr, dtype=np.uint16).reshape((self.max_detectable_obj, 
                                                                                             5))
        shared_object_certainty_data = np.frombuffer(shared_object_certainty_data_ptr, dtype=np.float32)
        shared_ultrasonic_data = np.frombuffer(shared_ultrasonic_data_ptr.get_obj(), dtype=np.float32)
        shared_img_data_cpy = np.frombuffer(shared_image_data_cpy_ptr, dtype=np.uint8).reshape(self.img_roi[3], 
                                                                                           self.img_roi[2], 3)
        shared_lane_data_cpy = np.frombuffer(shared_lane_data_cpy_ptr, 
                                             dtype=np.uint16).reshape((self.max_detectable_lanes, 4))
        shared_object_data_cpy = np.frombuffer(shared_object_data_cpy_ptr, 
                                               dtype=np.uint16).reshape((self.max_detectable_obj, 5))
        shared_object_certainty_data_cpy = np.frombuffer(shared_object_certainty_data_cpy_ptr, dtype=np.float32)
        shared_ultrasonic_data_cpy = np.frombuffer(shared_ultrasonic_data_cpy_ptr, dtype=np.float32)

        # wait until all processes are ready on startup
        self.logger.debug("Waiting for Startup...")
        self.startup_barrier.wait()

        while True:
            # wait until new image has been captured
            self.logger.debug("Waiting for E_4...")
            self.data_copy_event.wait()
            self.logger.debug("E_4 set. Copying data...")
            
            # wait, if stop signal was given by remote
            if not self.stop_event.is_set():
                self.logger.info("Stopped until remote gives signal to proceed.")
            self.stop_event.wait() 

            # copy data
            np.copyto(shared_img_data_cpy, shared_img_data)
            np.copyto(shared_lane_data_cpy, shared_lane_data)
            np.copyto(shared_object_data_cpy, shared_object_data)
            np.copyto(shared_object_certainty_data_cpy, shared_object_certainty_data)
            np.copyto(shared_ultrasonic_data_cpy, shared_ultrasonic_data)

            # reset waiting event
            self.logger.debug("Data copied. Cleared E_4.")
            self.data_copy_event.clear()
            
            # notify transmitter
            self.data_sending_event.set()
            self.logger.debug("Set E_5.")
            
            # notify camera
            self.image_capture_event.set()
            self.logger.debug("Set E_1.")

    def _on_termination(self):
        """Is invoked on process termination."""
        
        pass