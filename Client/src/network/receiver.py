#!/usr/bin/env python3

"""Implements Receiver (TCP) for Jetson."""

from collections import deque
import json
from json.decoder import JSONDecodeError
import socket
from typing import Any, Deque, List

import netifaces
import numpy as np

from process_wrapper import ProcessWrapper


class Receiver(ProcessWrapper):
    """Implements tcp receiver on client side, which is executed in a separate process."""

    def __init__(self, shared_control_speed_data_ptr: Any, shared_control_steering_data_ptr: Any, 
                 shared_manual_control_data_ptr: Any, shared_remote_ip_address_data_ptr: Any, 
                 control_application_event: Any, connection_event: Any, manual_control_event: Any, stop_event: Any, 
                 emergency_stop_event: Any, startup_barrier: Any, tcp_port: int):
        """Initializes TCP client.

        Args:
            shared_control_speed_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for speed value
            shared_control_steering_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for steering 
            value
            shared_manual_control_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for manual 
            control flag
            shared_remote_ip_address_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint8]): Shared memory 
            pointer for remote IP address
            control_application_event (multiprocessing.Event): Event for control application initiation
            connection_event (multiprocessing.Event): Event for initial tcp connection success
            manual_control_event (multiprocessing.Event): Event for safe switch from autonomous to manual control
            stop_event (multiprocessing.Event): Event for stop signal
            emergency_stop_event (multiprocessing.Event): Event for emergency stop signal
            startup_barrier (multiprocessing.Barrier): Barrier that synchronizes startup of all processes
            tcp_port (int): Port for TCP service
        """
        
        self.tcp_port = tcp_port

        # init events and barriers
        self.control_application_event = control_application_event
        self.connection_event = connection_event
        self.manual_control_event = manual_control_event
        self.stop_event = stop_event
        self.emergency_stop_event = emergency_stop_event
        self.startup_barrier = startup_barrier
        
        # init process
        shared_memory_pointers = (shared_control_speed_data_ptr, shared_control_steering_data_ptr,
                                  shared_manual_control_data_ptr, shared_remote_ip_address_data_ptr)
        super().__init__(shared_memory_pointers, emergency_stop_event)
 
    def _init(self):
        """Initializes TCP Receiver process specifically."""
         
        # get own ip address
        self.logger.info("Fetching current IPv4 address...")
        jetson_ip = netifaces.ifaddresses('wlan0')[netifaces.AF_INET][0]['addr']
        self.logger.info("IPv4 address has been fetched.")

        # init tcp socket
        self.logger.info("Initializing TCP socket...")
        self.tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcp_socket.bind((jetson_ip, self.tcp_port))
        self.logger.info("TCP socket has been initialized.")

    def _run(self, shared_control_speed_data_ptr: Any, shared_control_steering_data_ptr: Any, 
            shared_manual_control_data_ptr: Any, shared_remote_ip_address_data_ptr: Any):
        """
        Cyclic worker method, receives data and reacts to included commands.

        Args:
            shared_control_speed_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for speed value
            shared_control_steering_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for steering 
            value
            shared_manual_control_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for manual 
            control flag
            shared_remote_ip_address_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint8]): Shared memory 
            pointer for remote IP address
        """

        manual_control = None
        recv_buffer = deque(maxlen=5)
        recv_data = None
        json_obj = None
        data = None

        shared_remote_ip_address_data = np.frombuffer(shared_remote_ip_address_data_ptr, dtype=np.uint8)

        # wait for tcp connection establishment
        self.logger.info("Waiting for TCP connection...")
        self.tcp_socket.listen(1)
        self.tcp_connection, (remote_ip, _) = self.tcp_socket.accept()
        self.logger.info("TCP connection has been established.")
        # write ip address of remote to shared memory -> izmq transmitter process can use it to connect
        remote_ip_bytes = np.frombuffer(remote_ip.encode(), dtype=np.uint8)
        shared_remote_ip_address_data[0] = len(remote_ip_bytes)
        shared_remote_ip_address_data[1:len(remote_ip_bytes)+1] = remote_ip_bytes
        
        self.connection_event.set()

        # wait until all processes are ready on startup
        self.logger.debug("Waiting for Startup...")
        self.startup_barrier.wait()

        while True:
            # receive data from remote
            self.logger.debug("Receiving data...")
            recv_data = self.tcp_connection.recv(1024)
            # if connection is broken, trigger emergency stop signal
            if not recv_data:
                self.logger.error("TCP Connection was interrupted.")
                self.emergency_stop_event.set()
                break
            self.logger.debug("Data received")
            
            # add json string to receiving buffer and fetch most recent and complete json object
            recv_buffer.append(recv_data.decode())
            json_obj = self._get_most_recent_complete_json_obj(recv_buffer)
            if not json_obj:
                self.logger.warning("Only fragmental data received!")
                continue
            # decode json
            try:
                data = json.loads(json_obj)
            except JSONDecodeError:
                self.logger.error("JSON Decoding Error: %s", json_obj)
            
            # look for activated stop flag, possibly pause all processes except this one and the main process
            if int(data["STOP"]):
                if self.stop_event.is_set():
                    self.logger.info("Received stopping signal from remote. Stopping all active processes...")
                self.stop_event.clear()
            else:
                if not self.stop_event.is_set():
                    self.logger.info("Received starting signal from remote. Resuming all stopped processes...")
                self.stop_event.set()
                
            # get current control mode (decrease number of accesses due to resource locking costs)
            manual_control = shared_manual_control_data_ptr.value    
            
            # if manual control is activated, update parameters and notify car controller process
            if manual_control:
                self.logger.debug("Manual control enabled.")
                shared_control_speed_data_ptr.value = int(data["ENGINE"])
                shared_control_steering_data_ptr.value = int(data["STEER"])
                self.control_application_event.set()
                self.logger.debug("Set E_3.")
            
            # if currently driving autonomously and manual control signal is received from remote or vice versa, ensure 
            # safe switch between control modes -> switch is performed, when the camera process is entered the next time
            if manual_control != int(data["MAN_CTRL"]):
                self.manual_control_event.set()
            
            log_line = ["%s: %s" % (key, value) for key, value in data.items()]
            self.logger.info("; ".join(log_line))

    def _on_termination(self):
        """Is invoked on process termination. Disconnects from Remote TCP Sender and releases socket resources."""
        
        # check if connection is established
        if self.tcp_connection is not None and self.tcp_connection.fileno() != -1:
            # disconnect
            self.logger.info("Closing TCP connection...")
            self.tcp_connection.shutdown(socket.SHUT_RDWR)
            self.tcp_connection.close()
            self.logger.info("TCP connection has been closed.")

        # check if socket is set up
        if self.tcp_socket is not None:
            # release resources
            self.logger.info("Closing TCP socket...")
            self.tcp_socket.shutdown(socket.SHUT_RDWR)
            self.tcp_socket.close()
            self.logger.info("TCP socket has been closed.")
            
    def _get_most_recent_complete_json_obj(self, recv_buffer: Deque[str]) -> str:
        """
        Fetches most recent and complete json object from receiving buffer.
        
        Args:
            recv_buffer (Deque[str]): Receiving buffer

        Returns:
            str: Most recent and complete json object
        """
        
        # concatenate all received json fragments
        buffer_concat = "".join(recv_buffer)
        # split, where a json objects ends and another begins
        json_partitions = self._split_between(buffer_concat, "}{")
        # remove fragments, which can only be at the beginning or at the end
        if not json_partitions[0].startswith("{"):
            json_partitions = json_partitions[1:]
        if not json_partitions[-1].endswith("}"):
            json_partitions = json_partitions[:-1]
        
        if len(json_partitions) > 0:
            return json_partitions[-1]
        else:
            return ""
            
    @staticmethod
    def _split_between(input: str, chars: str) -> List[str]:
        """
        Splits string between two given characters (cf. https://stackoverflow.com/a/45702064).

        Args:
            input (str): String input
            chars (str): Two partitioning characters

        Returns:
            List[str]: List of input partitions
        """

        result_list = [chars[1] + line + chars[0] for line in input.split(chars)]

        result_list[0] = result_list[0][1:]
        result_list[-1] = result_list[-1][:-1]

        return result_list
