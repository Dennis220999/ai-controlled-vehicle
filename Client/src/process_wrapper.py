#!/usr/bin/env python3

"""Implements ProcessWrapper for Jetson."""

from enum import Enum
import logging
from multiprocessing import Process
import signal
import sys
from typing import Any, Tuple

from logging_ import logging_factory, logging_redirection


class ProcessWrapper:
    """
    Abstract wrapper class for multiprocessing.Process. It's necessary, since only run() method of Process is executed 
    in a separate process (and not the whole Process object with its attributes,
    cf. https://docs.python.org/3/library/multiprocessing.html#multiprocessing.Process.start). Thus, shared memory 
    pointers must be directly passed to run() method as parameters in order to provide access to the same memory 
    location (otherwise, the pointer would be located in the closed memory area of original process, being unaccessible
    for separate process). Apparently, this behavior doesn't occur for other objects like dictionaries or events (they
    are indeed accessible!), but only for shared memory pointers, as if they are secured in a special manner. The 
    original run() method of Process, however, does unfortunately not accept further parameters and therefore cannot be
    overridden as might be desired. This class provides a simple wrapper for passing shared memory pointers to new 
    processes, solving the mentioned problem.
    """
    
    DEBUG_MODE = False

    def __init__(self, shared_memory_pointers: Tuple, emergency_stop_event: Any):
        """
        Initializes ProcessWrapper.

        Args:
            init_args (Tuple): Arguments needed for process-specific initialization
            shared_memory_pointers (Tuple): Shared memory pointers
            emergency_stop_event (multiprocessing.Event): Event for emergency stop signal
        """
        
        # initialize logger
        logging_name = self.__class__.__name__
        if self.DEBUG_MODE:
            self.logger = logging_factory.create_logger(logging_name, logging.DEBUG)
        else:
            self.logger = logging_factory.create_logger(logging_name)
        
        self.emergency_stop_event = emergency_stop_event
        
        # initialize process
        self.process = Process(target=self._run_logged, args=shared_memory_pointers)

    def start(self):
        """Starts process."""

        self.process.start()

    def terminate(self):
        """Terminates process."""

        self.process.terminate()
        
    def _init(self):
        """
        Abstract init method that needs to be overridden for concrete processes.
        
        Raises:
            NotImplementedError: Needs to be implemented for concrete subclass
        """
        
        raise NotImplementedError
    
    def _handle_sigterm(self, signum: Enum, frame: Any):
        """
        Handles incoming SIGTERM signals, caused by invocing 'terminate()'. Ensures process-specific resource releasing,
        before the process is eventually terminated.

        Args:
            signum (Enum): Signal identifier (here: SIGTERM=15)
            frame (Frame Object): Current code frame
        """
        
        # first, release process-specific resources
        self._on_termination()
        # then, terminate
        sys.exit()
            
    def _run(self, *args: Any):
        """
        Abstract worker method that needs to be overridden for concrete processes.
        
        Args:
            args (Tuple): Arguments of the worker method
        Raises:
            NotImplementedError: Needs to be implemented for concrete subclass
        """

        raise NotImplementedError

    def _run_logged(self, *args: Any):
        """
        Logged wrapper for the worker method.
        
        Args:
            args (Tuple): Arguments of the worker method
        """
        
        # redirect SIGTERM to custom handler, ensuring process-specific resource releasing
        signal.signal(signal.SIGTERM, self._handle_sigterm)
        
        try:
            # TODO: Insert init in worker method
            self._init()
            self._run(*args)
        except Exception:
            logging_redirection.redirect_exception(self.logger)(*sys.exc_info())
            # trigger emergency stop signal
            self.emergency_stop_event.set()
            
    def _on_termination(self):
        """
        Abstract method that needs to be overridden for concrete processes. It is invoked just before process 
        termination.
        
        Raises:
            NotImplementedError: Needs to be implemented for concrete subclass
        """
        
        raise NotImplementedError