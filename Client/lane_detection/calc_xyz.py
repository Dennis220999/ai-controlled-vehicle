
"""The class CameraTransformation implements transformation from image to world 
coordinate system by using with numba precompiled functions."""
import numpy as np
import numba

# ORIENTATION OF WORLD COORDINATE SYSTEM
#   X: pointing to the left (parallel to the camera lense)
#   Y: pointing to the front of the car ("horzontal direction" of camera)
#   Z: pointing downwards

# constants 
ROT_MATRIX_180_DEG_Z_33 = np.array([[-1, 0, 0], [0, -1, 0], [0, 0, 1]], dtype=np.float32)
TRANSLATE_PATTERN_CENTER = np.array([-0.08, -0.06, 0], dtype=np.float32) 
CAM_F_WORLD = np.array([0.08149753,  0.51926134, -0.16415225], dtype=np.float32) 
XY_PLANE_NORMAL = np.array([0,0,1], dtype=np.float32)
ORIGIN = np.array([0,0,0], dtype=np.float32)

class CameraTransformer():
    """Implements transformation from image to world coordinate system and loads 
    all required parameters. An instance of this class can be passed to processes,
    which need to map from image to world coordinate system.
    """
    
    def __init__(self, img_width: int, img_height: int) -> None:
        """Initialize transformer by loading all required data from files.

        Args:
            img_width (int): initial width of image (before cropping)
            img_height (int): initial height of image (before cropping)
        """
        self.initial_size = np.array([img_width, img_height], dtype=np.uint16)
        
        self.roi = np.genfromtxt("../cam_calib/params/roi.csv")
        self.intrinsic_cam_matrix_33 = np.genfromtxt("../cam_calib/params/scaled_intrinsic_matrix.csv", delimiter=",", dtype=np.float32)
        self.inv_intrinsic_cam_matrix_33 = np.linalg.inv(self.intrinsic_cam_matrix_33).astype(np.float32)
        self.inv_intrinsic_cam_matrix_34 = np.vstack((self.inv_intrinsic_cam_matrix_33, np.array([[0,0,1]])))
        self.extrinsic_cam_matrix_44 = np.genfromtxt("../cam_calib/params/extrinsic_matrix.csv", delimiter=",", dtype=np.float32)
        self.inv_extrinsic_cam_matrix_44 = np.linalg.inv(self.extrinsic_cam_matrix_44).astype(np.float32)
        
    def conv_to_homogenous(self, point: np.ndarray) -> np.ndarray:
        """Convert n-dimensional coordinates the homogenous coordinates (n+1-dimensional)

        Args:
            point(np.array): nx1 array
        
        Returns:
            np.array: point in homogenous coordinates
        """
        return _conv_to_homogenous(point)

    def calc_xy0_camera_center(self, point_2d: np.ndarray) -> np.ndarray:
        """Calculate intersection of line from camera to point, determined by 
        projection of image coordinates to world space (point on ground beneath 
        the camera as origin of world coordinate system), and the x-y-plane.
    
        Args:
            point_2d(np.array): pixel coordinates of point to project to world
                                coordinate system (pixel coordinates in 
                                undistorted, not already cropped image)
    
        Returns:
            np.array: point in world coordinate system"""
        res = _calc_xy0_calib_board_center(point_2d, self.inv_intrinsic_cam_matrix_33,  
                                           self.inv_extrinsic_cam_matrix_44)
        res += np.array([0, CAM_F_WORLD[1], 0])
        return res

@numba.njit
def _calc_xy0_camera_center(point_2d: np.ndarray, inv_camera_matrix_33: np.ndarray, inv_extrinsic_matrix_44: np.ndarray) -> np.ndarray:
    """Refer to docstring of CameraTransformer.calc_xy0_camera_center."""
    res = _calc_xy0_calib_board_center(point_2d, inv_camera_matrix_33,  
                                       inv_extrinsic_matrix_44)
    res += np.array([0, CAM_F_WORLD[1] , 0])
    return res

@numba.njit
def _conv_to_homogenous(point: np.ndarray) -> np.ndarray:
    """Refer to docstring of CameraTransformer.conv_to_homogenous."""
    point_homogenous = np.hstack((point, np.array([1])))
    return point_homogenous.astype(np.float32)


@numba.njit
def _calc_intersection(point1, cam_f_world, plane_normal, plane_point, epsilon=1e-8):
    """Calculates intersection of line from point 1 to point 2 with the 
    specified plane.

    Args:
        point1(np.array): first 3D point (on the line)
        point2(np.array): second 3D point (on the line)
        plane_normal(np.array): normal vector of plane
        plane_point(np.array): any point in the plane
    
    Returns:
        np.array: intersection point between point and plane
    """
    direction_vector = point1 - cam_f_world 
    ndotu = plane_normal.dot(direction_vector)

    if abs(ndotu) < epsilon:
        raise RuntimeError("no intersection or line is within plane")

    w = point1 - plane_point
    si = -plane_normal.dot(w) / ndotu
    Psi = w + si * direction_vector + plane_point
    return Psi

@numba.njit
def _calc_xy0_calib_board_center(point_2d: np.ndarray, inv_intrinsic_cam_33: np.ndarray, 
                                 inv_extrinsic_cam_44: np.ndarray) -> np.ndarray:
    """Calculate intersection of line from camera to point, determined by 
    projection of image coordinates to world space (center of calibration pattern as origin
    of world coordinate system), and the x-y-plane.
    
    Args:
        point_2d(np.array): pixel coordinates of point to project to world
                            coordinate system (pixel coordinates in undistorted, not 
                            already cropped image)
        inv_intrinsic_cam_33 (np.ndarray):  inverted intrinsic camera matrix (3 x 3)
        inv_intrinsic_cam_34 (np.ndarray):  inverted intrinsic camera matrix (4 x 4)
    
    Returns:
        np.array: point in world coordinate system"""
    # convert pixel coordinates to homogenous coordinates
    point_homogenous_f_img = _conv_to_homogenous(point_2d)

    # convert homogenous pixel coordinates to homogenous camera coordinates
    point_f_cam = inv_intrinsic_cam_33.dot(point_homogenous_f_img)
    point_homogenous_f_cam =  _conv_to_homogenous(point_f_cam)

    # convert homogenous camera coordinates to world coordinates
    point_f_world = inv_extrinsic_cam_44.dot(point_homogenous_f_cam)
    point_f_world = point_f_world[:-1]

    # calculate intersection between line from camera to determined point and x-y-plane
    point_f_world = _calc_intersection(point_f_world, CAM_F_WORLD, XY_PLANE_NORMAL, ORIGIN)

    # translate points to set center of pattern size as origin of world coordinate system
    point_f_world += TRANSLATE_PATTERN_CENTER

    # rotate around z axis by 180 to ensure positive x axis pointing to the front
    point_f_world = point_f_world @ ROT_MATRIX_180_DEG_Z_33
    return point_f_world
