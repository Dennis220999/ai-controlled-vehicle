#!/usr/bin/env python3

"""Implements GUI for camera stream on remote server (laptop)."""

import os

from PyQt5.QtCore import pyqtSlot, Qt
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QMainWindow
from PyQt5 import uic

from network.izmq_receiver import ImageZmqReceiver

ui_camstreamwindow, qt_baseclass = uic.loadUiType(
    os.path.join(os.path.dirname(__file__), "../../ui", "cam_stream_window.ui"))


class CamStreamWindow(QMainWindow):
    """Class for Camera Stream Window"""

    def __init__(self, izmq_receiver: ImageZmqReceiver):
        """
        Initializes CamStreamWindow.

        Args:
            izmq_receiver (ImageZmqReceiver): ImageZMQ Receiver instance
        """

        super().__init__()
        self.ui = ui_camstreamwindow()
        self.ui.setupUi(self)

        # connect ImageZMQ Receiver with GUI
        self.izmq_receiver = izmq_receiver
        self.izmq_receiver.qimg_changed.connect(self.on_qimg_changed)

    @pyqtSlot(QImage)
    def on_qimg_changed(self, q_image: QImage):
        """
        Slot-method being executed when new image has been passed by ImageZMQ Receiver. Updates GUI image view.

        Args:
            q_image (QImage): Received image
        """

        # get current view size
        width, height = self.ui.image_label.frameGeometry().width(), self.ui.image_label.frameGeometry().width()
        # scale image accordingly
        q_image = q_image.scaled(width, height, Qt.KeepAspectRatio)
        # convert to pixmap and draw in GUI
        pixmap = QPixmap.fromImage(q_image)
        self.ui.image_label.setPixmap(pixmap)
