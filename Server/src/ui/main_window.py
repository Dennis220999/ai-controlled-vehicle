#!/usr/bin/env python3

"""Implements GUI for MainWindow on remote server (laptop)."""

from input_controls.input_manager import InputManager
import os
import sys
from typing import Dict
from functools import partial

from PyQt5.QtWidgets import QMainWindow, QMessageBox
from PyQt5 import uic
from PyQt5.QtCore import pyqtSignal, pyqtSlot
import yaml
from time import sleep
from datetime import datetime

from network.tcp_sender import TCPSender
from network.izmq_receiver import ImageZmqReceiver
from ui.cam_stream_window import CamStreamWindow
from ui.log_highlighter import LogHighlighter

import logging 

ui_mainwindow, qt_baseclass = uic.loadUiType(
    os.path.join(os.path.dirname(__file__), "../../ui", "mainwindow.ui"))


class MainWindow(QMainWindow):
    """Class for MainWindow"""
    
    # signal being emitted for establishing/closing tcp connection 
    establish_tcp_connection = pyqtSignal(bool)
    # signal being emitted for starting/stopping Jetson 
    stop_state_changed = pyqtSignal(bool)
    # signal being emitted for enabling/disabling manual control 
    manual_control_enabled = pyqtSignal(bool)

    def __init__(self):
        """Initializes MainWindow and connects slots and signals."""
        
        # --- SETUP GUI AND GENERAL PARAMETER ---
        super().__init__()
        self.ui = ui_mainwindow()
        self.ui.setupUi(self)
        self.config_file_name = os.path.join(os.path.dirname(__file__), "../../config", "main_config.yml")
        self.highlighter = LogHighlighter(self.ui.status_textedit.document())
        self.__log_file = os.devnull # write log if not wished to dev null

        # --- CAM STREAM GUI ---
        self.ui.cam_stream_button.clicked.connect(self.on_cam_stream_button_click)
        self.cam_stream_window = None
        
        self.ui.control_toggle_button.clicked.connect(self.on_control_toggle_button_click)
        self.ui.manual_control_checkbox.stateChanged.connect(self.on_manual_control_enabled)

        # --- CONFIGURATIONS ---
        self.config = None
        self.cam_detected_lanes_shown = None
        self.cam_detected_objects_shown = None
        self.enabled_log_components = None
        self.speed_boundaries = None
        self.steering_boundaries = None
        # add other configurations with initialization None here, the concrete
        # values should be read in self.read_config()
        config_reading_successful = self.read_config()
        # flag for Jetson start/stop (control)
        self.jetson_started = False

        # --- CONNECT SIGNALS FOR STATUSBAR ---
        self.ui.action_connect.triggered.connect(self.connect_tcp)
        self.ui.action_disconnect.triggered.connect(self.disconnect_tcp)
        self.ui.log_steering.triggered.connect(partial(self.set_log_option, "LOG_STEERING"))
        self.ui.log_engine.triggered.connect(partial(self.set_log_option, "LOG_ENGINE"))
        self.ui.log_detected_objects.triggered.connect(partial(self.set_log_option, "LOG_DETECTED_OBJECTS"))
        self.ui.log_detected_lanes.triggered.connect(partial(self.set_log_option, "LOG_DETECTED_LANES"))
        self.ui.log_distance_control.triggered.connect(partial(self.set_log_option, "LOG_DISTANCE_CONTROL"))
        self.ui.log_frequencies.triggered.connect(partial(self.set_log_option, "LOG_FREQUENCIES"))

        # --- SHOW GUI OR STOP IF CONFIG NOT FOUND ---
        if config_reading_successful:
            # setup ImageZMQ Receiver
            self.izmq_receiver = ImageZmqReceiver(int(self.config["CONNECTION"]["IZMQ_PORT"]))
            self.izmq_receiver.msg_data_received.connect(self.on_msg_data_received)
            self.ui.detected_lanes_checkbox.stateChanged.connect(
                self.izmq_receiver.on_lanes_enabled)
            self.ui.detected_objects_checkbox.stateChanged.connect(
                self.izmq_receiver.on_bounding_boxes_enabled)
            self.izmq_receiver.start()
            # setup TCP Sender
            self.tcp_sender = TCPSender(self, self.config["CONNECTION"]["CLIENT_ADDRESS"],
                                                          int(self.config["CONNECTION"]["TCP_PORT"]),
                                                          self.speed_boundaries, self.steering_boundaries)
            self.establish_tcp_connection.connect(self.tcp_sender.establish_connection)
            self.stop_state_changed.connect(self.tcp_sender.on_stop_state_changed)
            self.manual_control_enabled.connect(self.tcp_sender.on_manual_control_enabled)
            # setup InputManager
            self.input_manager = InputManager(self, self.tcp_sender, self.speed_boundaries, self.steering_boundaries)
            
            # --- Set up logger --- 
            self.logger = logging.getLogger(__name__)
            self.logger.setLevel(logging.DEBUG) # general logger level to highest, so no filter applied
            self.configure_logger()
            
            # show main window
            self.adapt_ui_to_config()
            self.show()
        else:
            self.close()

    # --- METHODS FOR MANAGING CONFIGURATION ---
    def read_config(self) -> bool:
        """
        Reads configuration file from config/main_config.yml.

        Returns:
            bool: Read-in success
        """

        # check, if config file exists
        if not os.path.isfile(self.config_file_name):
            self.warning("Config file not found",
                         "Please check for the existence of the file %s" %
                         self.config_file_name)
            return False

        # parse config file
        with open(self.config_file_name, 'r') as config_stream:
            self.config = yaml.safe_load(config_stream)

            # take config data
            self.cam_detected_lanes_shown = int(self.config["CAM"]["CAM_DETECTED_LANES"])
            self.cam_detected_objects_shown = int(self.config["CAM"]["CAM_DETECTED_OBJECTS"])
            self.enabled_log_components = [log_component for log_component, enabled
                                           in self.config["LOG_COMPONENTS"].items()
                                           if int(enabled)]
            self.speed_boundaries = (int(self.config["CAR_CONTROLLER"]["ENGINE"]["MAX_SPEED_BACK"]), 
                                     int(self.config["CAR_CONTROLLER"]["ENGINE"]["MAX_SPEED_FRONT"]))
            self.steering_boundaries = (int(self.config["CAR_CONTROLLER"]["STEER"]["MAX_LEFT"]), 
                                     int(self.config["CAR_CONTROLLER"]["STEER"]["MAX_RIGHT"]))

            return True

    def write_config(self) -> bool:
        """
        Writes config to config/main_config.yml.

        Returns:
            bool: Write success
        """

        if not os.path.isfile(self.config_file_name):
            self.warning("Could not find config file",
                         "Configuration can not be written to config file. "
                         "Check for existence of config file.")
            return False

        self.config["CAM"]["CAM_DETECTED_LANES"] = int(self.ui.detected_lanes_checkbox.isChecked())
        self.config["CAM"]["CAM_DETECTED_OBJECTS"] = int(self.ui.detected_objects_checkbox.isChecked())
        for config_opt in self.config["LOG_COMPONENTS"]:
            self.config["LOG_COMPONENTS"][config_opt] = int(getattr(self.ui, config_opt.lower()).isChecked())
        with open(self.config_file_name, "r+") as config_stream:
            yaml.dump(self.config, config_stream, default_flow_style=False)

        return True

    # --- METHODS FOR TCP SENDER ---
    def connect_tcp(self):
        """Connects TCP sender to Jetson."""
            
        self.establish_tcp_connection.emit(True)
        self.ui.action_connect.setEnabled(False)
        self.ui.action_disconnect.setEnabled(True)
        
    def disconnect_tcp(self):
        """Disconnects TCP sender fron Jetson."""

        self.establish_tcp_connection.emit(False)
        self.ui.action_connect.setEnabled(True)
        self.ui.action_disconnect.setEnabled(False)

    # --- METHODS FOR GUI ADAPTION ---
    def adapt_ui_to_config(self):
        """Adapts UI to read-in config data."""

        self.ui.detected_lanes_checkbox.setChecked(self.cam_detected_lanes_shown)
        self.ui.detected_objects_checkbox.setChecked(self.cam_detected_objects_shown)
        for log_component in self.enabled_log_components:
            action_item = getattr(self.ui, log_component.lower())
            action_item.setChecked(True)
    
    @pyqtSlot(str)
    def set_log_option(self, key: str):
        """Sets / resets an filter option for the log. 

        Args:
            key(str): key in MainWindow.config["LOG_COMPONENTS"]"""
        
        if key in self.config["LOG_COMPONENTS"]:
            self.config["LOG_COMPONENTS"][key] = not self.config["LOG_COMPONENTS"][key]

    def on_cam_stream_button_click(self):
        """Slot-method being executed when clicking on "Open Stream" button."""

        self.cam_stream_window = CamStreamWindow(self.izmq_receiver)
        self.cam_stream_window.show()
        
    def on_control_toggle_button_click(self):
        """Slot-method being executed when clicking on "Start/Stop" button."""
        
        self.jetson_started = not self.jetson_started
        
        if self.jetson_started:
            self.ui.control_toggle_button.setText("Stop")
        else:
            self.ui.control_toggle_button.setText("Start")
        
        self.stop_state_changed.emit(not self.jetson_started)
            
    def on_manual_control_enabled(self, manual_control_enabled: int):
        """
        Slot-method being executed when manual control should be enabled/disabled due to checkbox checking 
        state change.

        Args:
            manual_control_enabled (int): Whether to control Jetson manually or not
        """

        self.input_manager.running = bool(manual_control_enabled)
        if self.input_manager.running:
            self.input_manager.start()
        
        self.manual_control_enabled.emit(bool(manual_control_enabled))
        
    def on_msg_data_received(self, msg_data: Dict, recv_freq: float):
        """
        Slot-method being executed when new message data has been passed by ImageZMQ Receiver. Updates GUI status log.

        Args:
            msg_data (Dict): Received data
            recv_freq (float): Current receiving frequency
        """
        
        time = msg_data["TIME"]

        console_log, file_log = "[TIMESTAMP]: %s\n" % time, "[TIMESTAMP]: %s\n" % time
        
        block = "[WORKING FREQUENCY]: %s Hz\n" % (msg_data["WORK_FREQ"])
        block += "[RECEIVING FREQUENCY]: %.2f Hz\n\n" % (recv_freq)

        file_log += block
        if self.config["LOG_COMPONENTS"]["LOG_FREQUENCIES"]:
            console_log += block
        
        block = "[ENGINE]: %s\n\n" % (msg_data["ENGINE"])

        file_log += block
        if self.config["LOG_COMPONENTS"]["LOG_ENGINE"]:
            console_log += block
        
        block = "[STEERING]: %s\n\n" % (msg_data["STEER"])

        file_log += block
        if self.config["LOG_COMPONENTS"]["LOG_STEERING"]:
            console_log += block
        
        block = ""
        for sensor_name, distance in msg_data["US"].items():
            block += "[DISTANCE CONTROL %s]: %s cm\n\n" % (sensor_name, distance)

        file_log += block
        if self.config["LOG_COMPONENTS"]["LOG_DISTANCE_CONTROL"]:
            console_log += block
        
        block = ""
        for detected_lane in msg_data["LANE_DETC"]:
            block += "[LANE DETECTION]: (x1=%s, y1=%s), (x2=%s, y2=%s)\n" \
                % ( detected_lane["X1"], 
                    detected_lane["Y1"],
                    detected_lane["X2"],
                    detected_lane["Y2"])

        file_log += block
        if self.config["LOG_COMPONENTS"]["LOG_DETECTED_LANES"]:
            console_log += block
            
        block = ""
        for detected_obj in msg_data["OBJ_DETC"]:
            block += "[OBJECT DETECTION]: %s | %s %% | (x0=%s, y0=%s, x1=%s, y1=%s)\n" % (
                detected_obj["NAME"], detected_obj["CERT"], detected_obj["BB_X0"], 
                detected_obj["BB_Y0"], detected_obj["BB_X1"], detected_obj["BB_Y1"])
        
        file_log += block
        if self.config["LOG_COMPONENTS"]["LOG_DETECTED_OBJECTS"]:
            console_log += block

        self.ui.status_textedit.setPlainText(console_log)
        self.ui.status_textedit.ensureCursorVisible()

        for line in file_log.splitlines():
            if len(line) > 1:
                self.logger.info(line)
        
        self.logger.info(" ")
        self.logger.info("--------------------------------------------------------")
        self.logger.info(" ")
        
        
    def warning(self, title: str, message: str):
        """
        Shows warning in popup.

        Args:
            title (str): Title for warning box
            message (str): Message for warning box
        """
        
        message_box = QMessageBox()
        message_box.critical(self, title, message)

    def closeEvent(self, event):
        """Overwrites closeEvent-Method. It is executed when GUI is closed."""
        
        # update config before window closing
        self.write_config()
        if self.cam_stream_window is not None:
            self.cam_stream_window.close()
        event.accept()

    def configure_logger(self):
        # setup console logger
        ch = logging.StreamHandler(stream=sys.__stdout__)
        ch.setLevel(self.config["LOGGING_PC"]["CONSOLE_LEVEL"])
        formatter = logging.Formatter('[%(levelname)s]:    %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

        if self.config["LOGGING_PC"]["FILE_LEVEL"] != 'DEACTIVE':
            fh = logging.FileHandler(str(datetime.now().strftime("%Y-%m-%d_%H-%M") + ".txt"), mode='a')
            fh.setLevel(self.config["LOGGING_PC"]["FILE_LEVEL"])
            fh.setFormatter(formatter)
            self.logger.addHandler(fh)
        
