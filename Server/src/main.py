##!/usr/bin/env python3

"""Opens GUI for remote control on remote server (laptop)."""

import sys

from PyQt5.QtWidgets import QApplication

from ui.main_window import MainWindow


if __name__ == '__main__':
    app = QApplication(sys.argv)

    main = MainWindow()
    main.show()

    sys.exit(app.exec_())
