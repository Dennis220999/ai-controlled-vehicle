#!/usr/bin/env python3

"""Implements InputManager on remote server (laptop)."""

from typing import Tuple

from inputs import get_gamepad, UnpluggedError
from PyQt5.QtCore import pyqtSignal, QThread
from PyQt5.QtWidgets import QMainWindow

from network.tcp_sender import TCPSender

class InputManager(QThread):
    """Class for InputManager"""
    
    # signal being emitted on speed input change
    speed_input_changed = pyqtSignal(int)
    # signal being emitted on steering input change
    steering_input_changed = pyqtSignal(int)
    # signal being emitted to show warning window
    show_warning_window = pyqtSignal(str, str)
    
    # Xbox 360 Gamepad constants
    JOYSTICK_X = "ABS_X"
    JOYSTICK_Y = "ABS_Y"
    JOYSTICK_RANGE = 2**16
    
    def __init__(self, main_window: QMainWindow, tcp_sender: TCPSender, speed_boundaries: Tuple[int, int], 
                 steering_boundaries: Tuple[int, int]):
        """
        Initializes InputManager

        Args:
            main_window (QMainWindow): MainWindow instance
            tcp_sender (TCPSender): TCPSender instance
            speed_boundaries (Tuple[int, int]): min and max permissable speed parameters
            steering_boundaries (Tuple[int, int]): min and max permissable steering parameters
        """
        
        super().__init__()
        
        self.main_window = main_window
        self.tcp_sender = tcp_sender
        self.speed_input_changed.connect(tcp_sender.on_manual_control_speed_changed)
        self.steering_input_changed.connect(tcp_sender.on_manual_control_steering_changed)
        self.show_warning_window.connect(main_window.warning)
        
        self.speed_range = speed_boundaries[1] - speed_boundaries[0]
        self.speed_neutral = (speed_boundaries[0] + speed_boundaries[1]) // 2
        self.steering_range = steering_boundaries[1] - steering_boundaries[0]
        self.steering_neutral = (steering_boundaries[0] + steering_boundaries[1]) // 2
        
        self.running = False
        
    def run(self):
        """Worker method, notices input control events and sends corresponding signals."""
        
        while self.running:
            try:
                events = get_gamepad()
                for event in events:
                    if event.code == self.JOYSTICK_X:
                        steering = self._get_steering_from_x_input(event.state)
                        self.steering_input_changed.emit(steering)
                    elif event.code == self.JOYSTICK_Y:
                        speed = self._get_speed_from_y_input(event.state)
                        self.speed_input_changed.emit(speed)
            except UnpluggedError:
                # if started, stop Jetson
                if self.main_window.jetson_started:
                    self.main_window.on_control_toggle_button_click()
                # deactivate manual control
                self.main_window.ui.manual_control_checkbox.setChecked(0)
                # show warning
                self.show_warning_window.emit("InputManager Error", "Gamepad not found!")
                break
    
    def _get_steering_from_x_input(self, x_input: int) -> int:
        """
        Maps horizontal joystick input value onto steering value.

        Args:
            x_input (int): horizontal joystick input value [−32,767; +32,767]
        Returns:
            int: steering value
        """
        
        return round(x_input * (self.steering_range / self.JOYSTICK_RANGE) + self.steering_neutral)
    
    def _get_speed_from_y_input(self, y_input: int) -> int:
        """
        Maps vertical joystick input value onto speed value.

        Args:
            y_input (int): vertical joystick input value [−32,767; +32,767]
        Returns:
            int: speed value
        """
        
        return round(y_input * (self.speed_range / self.JOYSTICK_RANGE) + self.speed_neutral)