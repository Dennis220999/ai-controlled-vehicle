#!/usr/bin/env python3

"""
Implements the TCP Sender for unidirectional communication with the client
(Jetson).
"""

from datetime import datetime
from enum import Enum
import json
from typing import Tuple

from PyQt5.QtCore import QIODevice
from PyQt5.QtNetwork import QAbstractSocket, QTcpSocket
from PyQt5.QtWidgets import QMainWindow


class ConnectionState(Enum):
    """
    Represents the connection states of TCP socket.
    cf. https://doc.qt.io/qt-5/qabstractsocket.html#SocketState-enum
    """
    
    Unconnected = 0
    HostLookup = 1
    Connecting = 2
    Connected = 3
    Bound = 4
    Listening = 5
    Closing = 6
        
    def get_color(self) -> str:
        """
        Returns corresponding color.
        
        Returns:
           str: Hex color code
        """
        
        if self is ConnectionState.Unconnected:
            return "#FFFFFF" # white
        elif self is ConnectionState.HostLookup or self is ConnectionState.Connecting:
            return "#FFFF00" # yellow
        elif self is ConnectionState.Connected:
            return "#00FF00" # green
        elif self is ConnectionState.Closing:
            return "#FF0000" # red
        else:
            return "#0000FF" # blue (which should not happen normally)
        

class ConnectionError(Enum):
    """
    Represents the connection errors of TCP socket. 
    cf. https://doc.qt.io/qt-5/qabstractsocket.html#SocketError-enum
    """
    
    ConnectionRefusedError = 0
    RemoteHostClosedError = 1
    HostNotFoundError = 2
    SocketAccessError = 3
    SocketResourceError = 4
    SocketTimeoutError = 5
    DatagramTooLargeError = 6
    NetworkError = 7
    AddressInUseError = 8
    SocketAddressNotAvailableError = 9
    UnsupportedSocketOperationError = 10
    ProxyAuthenticationRequiredError = 11
    SslHandshakeFailedError = 12
    UnfinishedSocketOperationError = 13
    ProxyConnectionRefusedError = 14
    ProxyConnectionClosedError = 15
    ProxyConnectionTimeoutError = 16
    ProxyNotFoundError = 17
    ProxyProtocolError = 18
    OperationError = 19
    SslInternalError = 20
    SslInvalidUserDataError = 21
    TemporaryError = 22
    UnknownSocketError = -1
    

class TCPSender:
    """
    Represents the TCP Sender that connects to the client over the specified port. Connection must be established 
    manually with TCPSender.establish_connection(). TCP Sender works asynchronously and is managed by QtNetwork.
    """
    
    def __init__(self, main_window: QMainWindow, client_address: str, tcp_port: int, speed_boundaries: Tuple[int, int], 
                 steering_boundaries: Tuple[int, int]):
        """
        Initializes the TCP Sender.

        Args:
            main_window (QMainWindow): MainWindow context
            client_address (str): IP address or hostname of client
            tcp_port (int): Port for TCP service
            neutral_speed (int): Port for TCP service
            neutral_steering (int): Port for TCP service
        """
        
        self.socket = QTcpSocket(main_window)

        self.main_window = main_window
        self.client_address = client_address
        self.tcp_port = tcp_port
        
        self.data = {"TIME": "",
                     "MAN_CTRL": 0,
                     "ENGINE": (speed_boundaries[0] + speed_boundaries[1]) // 2,
                     "STEER": (steering_boundaries[0] + steering_boundaries[1]) // 2,
                     "STOP": 0}
        
        # TODO: Default values for Engine and Steer
        
        self.socket.stateChanged.connect(self.on_socket_state_changed)
        self.socket.errorOccurred.connect(self.on_socket_error_occurred)

    def send_data(self):
        """Sends data to the client."""
        
        self.data["TIME"] = datetime.now().strftime("%H:%M:%S.%f")[:-3]
        databytes = json.dumps(self.data).encode()
        self.socket.write(databytes)
        self.socket.flush()
        
    def establish_connection(self, establish: bool):
        """
        Establishes or closes connection to Jetson.
        
        Args:
            establish (bool): Whether to establish or close connection.
        """

        if establish:
            self.socket.connectToHost(self.client_address, self.tcp_port, QIODevice.WriteOnly)
        else:
            self.socket.close()
        
    def on_stop_state_changed(self, stop: bool):
        """
        Slot-method being executed when Start/Stop toggle button has been clicked and stop state has changed.

        Args:
            stop (bool): Whether to start or stop Jetson
        """
        
        self.data["STOP"] = int(stop)
        self.send_data()
        
    def on_manual_control_enabled(self, manual_control_enabled: bool):
        """
        Slot-method being executed when manual control should be enabled/disabled due to checkbox checking 
        state change.

        Args:
            manual_control_enabled (bool): Whether to control Jetson manually or not
        """

        self.data["MAN_CTRL"] = int(manual_control_enabled)
        self.send_data()
        
    def on_manual_control_speed_changed(self, speed: int):
        """
        Slot-method being executed when manual control parameter for speed changed due to keyboard interaction.

        Args:
            speed (int): New speed value
        """
        
        self.data["ENGINE"] = speed
        self.send_data()
        
    def on_manual_control_steering_changed(self, steering: int):
        """
        Slot-method being executed when manual control parameter for steering changed due to keyboard interaction.

        Args:
            steering (int): New steering value
        """
        
        self.data["STEER"] = steering
        self.send_data()
    
    def on_socket_state_changed(self, q_state: QAbstractSocket.SocketState):
        """
        Slot-method being executed when TCP socket state has changed.

        Args:
            q_state (int): State ID
        """
        
        state = ConnectionState(q_state)
        color = state.get_color()
        state_expr = state.name.upper()
        self.main_window.ui.connection_status_label.setText(state_expr)
        self.main_window.ui.connection_status_label.setStyleSheet("background-color: %s; padding: 10px" % color)
        
        
    def on_socket_error_occurred(self, q_error: QAbstractSocket.SocketError):
        """
        Slot-method being executed when a TCP socket error has occured.

        Args:
            q_error (int): Error ID
        """
        
        error = ConnectionError(q_error)
        error_expr = error.name
        self.main_window.warning("TCP Sender Error", error_expr)
        self.main_window.disconnect_tcp()
