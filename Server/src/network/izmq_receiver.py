#!/usr/bin/env python3

"""Implements ImageZMQ Receiver on remote server (laptop)."""

from collections import deque
import json
import socket
import time
from typing import Dict

import cv2
import imagezmq
import numpy as np
from PyQt5.QtCore import pyqtSignal, QThread
from PyQt5.QtGui import QImage

MAX_TRAJECTORY_LENGTH_PX = 150  # is also specified in Client/src/control/plan_trajectory.py
MIN_VERTICAL_SLOPE = 0.2        # is also specified in Clien7/src/image_processing/join_lines.py

class ImageZmqReceiver(QThread):
    """Class for ImageZMQ Receiver"""

    # signal being emitted on new image receiving
    qimg_changed = pyqtSignal(QImage)
    # signal being emitted on message data receiving
    msg_data_received = pyqtSignal(dict, float)

    def __init__(self, izmq_port: int):
        """
        Initializes ImageZMQ Receiver.

        Args:
            izmq_port (int): ImageZMQ port for client/server connection
        """

        super().__init__()
        self.lanes_enabled = False
        self.bounding_boxes_enabled = False
        
        # get own ip address and craft uri
        ip_address = socket.gethostbyname(socket.gethostname())
        izmq_uri = "tcp://%s:%d" % (ip_address, izmq_port)
        
        # setup hub for ImageZMQ data receiving
        self.image_hub = imagezmq.ImageHub(izmq_uri, True)
        
        # initialize buffer for received images (preventing GC and Qt crashes)
        self.img_buffer = deque(maxlen=10)
        
        # close connection on thread termination
        self.finished.connect(lambda: self.image_hub.close())  

    def run(self):
        """Worker method, receives images and converts to QImage continuously."""
        
        # initial timestamp
        timestamp = time.time()
        old_timestamp = None

        while True:
            # receive new image (jpg) and text message (json)
            message, jpg_buffer = self.image_hub.recv_jpg()
            
            # measure current receiving frequency
            old_timestamp = timestamp
            timestamp = time.time()
            recv_freq = 1 / (timestamp - old_timestamp)
            
            # send reply to client
            self.image_hub.send_reply()
            # decode jpg, convert to OpenCV image
            image = cv2.imdecode(np.frombuffer(jpg_buffer, dtype='uint8'), -1)
            # decode json, convert to dictionary
            msg_data = json.loads(message)
            # if desired, draw lanes
            if self.lanes_enabled:
                image = self.draw_lanes(image, msg_data)
            # if desired, draw bounding boxes
            if self.bounding_boxes_enabled:
                image = self.draw_bounding_boxes(image, msg_data)
            # convert OpenCV image to QImage, add to buffer and notify GUI
            q_image = self.img_to_qimg(image)
            self.img_buffer.append(q_image)
            self.qimg_changed.emit(q_image)
            self.msg_data_received.emit(msg_data, recv_freq)

    @staticmethod
    def img_to_qimg(cv_image: np.ndarray) -> QImage:
        """
        Converts OpenCV image to QImage.

        Args:
            cv_image (np.ndarray[h, w, d][np.uint8]): OpenCV image

        Returns:
            QImage: Converted QImage
        """

        # adjust color space (OpenCV images are BGR per default, RGB is needed)
        rgb_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)
        # get height, width and bytes per pixel/chroma
        height, width, chroma = rgb_image.shape
        # compute bytes per image line
        bytes_per_line = chroma * width
        # convert OpenCV image to QImage
        q_image = QImage(rgb_image.data, width, height, bytes_per_line, QImage.Format_RGB888)
        return q_image
    
    def draw_lanes(self, cv_image: np.ndarray, msg_data: Dict) -> np.ndarray:
        """
        Draws lanes onto OpenCV image.

        Args:
            cv_image (np.ndarray[h, w, d][np.uint8]): OpenCV image
            msg_data (Dict): Received message data, containing lane data

        Returns:
            np.ndarray[h, w, d][np.uint8]: OpenCV image with lanes
        """
        # draw street lanes
        for detected_lane in msg_data["LANE_DETC"]:
            p1 = (int(detected_lane["X1"]), int(detected_lane["Y1"]))
            p2 = (int(detected_lane["X2"]), int(detected_lane["Y2"]))
            cv2.line(cv_image, p1, p2, (0, 0, 255), 3)
            
        # draw trajectory 
        start_point = np.array(cv_image.shape[0:2], np.uint16)
        start_point[0] -= 30
        start_point[1] = start_point[1] // 2
        trajectory_componets = self._map_angles_to_trajectory_components(msg_data["ENGINE"], msg_data["STEER"])
        trajectory_componets[0] = self._inverse_map_slope(trajectory_componets[0])
        
        trajectory_vector = self._calc_trajectory_vector(trajectory_componets)
        end_point = start_point - trajectory_vector
        cv2.line(cv_image, tuple(start_point.astype(int)), tuple(end_point.astype(int)), (255, 0, 0), 3)
        
        return cv_image

    @staticmethod
    def draw_bounding_boxes(cv_image: np.ndarray, msg_data: Dict) -> np.ndarray:
        """
        Draws bounding boxes of detected objects in OpenCV image.

        Args:
            cv_image (np.ndarray[h, w, d][np.uint8]): OpenCV image
            msg_data (Dict): Received message data, containing bounding boxes data

        Returns:
            np.ndarray[h, w, d][np.uint8]: OpenCV image with bounding boxes
        """
        for object in msg_data["OBJ_DETC"]:
            cv2.rectangle(cv_image,(object["BB_X0"], object["BB_Y0"]),
                                   (object["BB_X1"], object["BB_Y1"]),
                                   color=(0,255,0), thickness=2)
            if object["BB_X1"] <= (cv_image.shape[1] // 2):
                origin = (object["BB_X1"] + 5, object["BB_Y0"] + (object["BB_Y1"] - object["BB_Y0"]) // 2)
            else:
                origin = (object["BB_X0"] - 65, object["BB_Y0"] + (object["BB_Y1"] - object["BB_Y0"]) // 2)

            cv2.putText(cv_image,object["NAME"],origin,color=(255,255,0),fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.7, thickness=2)
            y = origin[1] + 25  # set distance estimation below name
            cv2.putText(cv_image,(f'{object["CERT"]:.2f} %'), (origin[0],y), color=(242, 80, 207),fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.5, thickness=2)

        return cv_image
    
    def on_lanes_enabled(self, lanes_enabled: int):
        """
        Slot-method being executed when lane drawing should be enabled/disabled due to checkbox checking 
        state change.

        Args:
            lanes_enabled (int): Whether to draw lanes or not
        """

        self.lanes_enabled = lanes_enabled

    def on_bounding_boxes_enabled(self, bounding_boxes_enabled: int):
        """
        Slot-method being executed when bounding boxes drawing should be enabled/disabled due to checkbox checking 
        state change.

        Args:
            bounding_boxes_enabled (int): Whether to draw bounding boxes or not
        """

        self.bounding_boxes_enabled = bounding_boxes_enabled

    def _map_angles_to_trajectory_components(self, current_speed: float, current_steering: float) -> np.ndarray:
        #TODO: make parameters dependant on constants
        trajectory_components = np.empty((2,), dtype=np.float32)
        trajectory_components[1] = (current_speed - 90) * MAX_TRAJECTORY_LENGTH_PX / 10
        trajectory_components[0] = (current_steering - 90) / 80
        return trajectory_components
    
    @staticmethod
    def _calc_trajectory_vector(trajectory_components: np.ndarray) -> np.ndarray:
        vector = np.array([-trajectory_components[0], 1])
        vector *= trajectory_components[1]
        return vector
    
    @staticmethod
    def _inverse_map_slope(slope: float) -> float:
        """Maps slope to values from -MIN_VERTICAL_SLOPE (slope = -1) to MIN_VERTICAL_SLOPE (slope = 1) - 
        slope = 0 matches value inf.

        Args:
            slope (float): slope to be mapped
            
        Returns:
            float: mapped slope value
        """
        if slope == 0.0:
            # return small value, because slope of exactly 0 implicits that line is of length 0
            return 1e-6
        else:
            return slope / MIN_VERTICAL_SLOPE
