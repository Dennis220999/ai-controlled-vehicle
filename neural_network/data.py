from typing import Callable, Tuple
import torch.nn.functional as F
from torch.utils import data
from torch.utils.data.dataloader import DataLoader
from torch.utils.data.dataset import Dataset
from torch.utils.data import random_split
from torchvision import datasets, transforms
from torchvision.transforms.transforms import Compose


# https://github.com/kuangliu/pytorch-cifar/issues/19
NORMALIZE_CIFAR10 = transforms.Normalize((0.4914, 0.4822, 0.4465), (0.247, 0.243, 0.261))

NORMALIZE_CUSTOM = transforms.Normalize((0.32175523, 0.31878638, 0.26313573), (0.11819864, 0.122116566, 0.13291316))

def compute_normalization_params(dataset: Dataset) -> Tuple[Tuple, Tuple]:
    """
    Computes normalization parameters (mean and std) for given dataset.

    Args:
        dataset (Dataset): Used dataset

    Returns:
        Tuple[Tuple[float, float, float], Tuple[float, float, float]]: Normalization parameters (mean, std)
    """
    
    loader = DataLoader(dataset, batch_size=len(dataset))
    
    x, y = next(iter(loader))
    
    mean = tuple(x.mean(dim=(0, 2, 3)).numpy())
    std = tuple(x.std(dim=(0, 2, 3)).numpy())
    
    print("MEAN", mean)
    print("STD", std)
    
    return mean, std


def split_data(dataset: Dataset, training_split_ratio: float) -> Tuple[Dataset, Dataset]:
    """
    Splits given dataset in training and validation data.

    Args:
        dataset (Dataset): Complete dataset
        training_split_ratio (float): Ratio for training data split

    Returns:
        Tuple[Dataset, Dataset]: Training dataset and validation dataset
    """
    
    train_size = int(training_split_ratio * len(dataset))
    valid_size = len(dataset) - train_size
    
    train_dataset, valid_dataset = random_split(dataset, [train_size, valid_size])
    
    return train_dataset, valid_dataset


def load_img_data(img_size: int, training_split_ratio: float, augmentation: bool, 
                  compute_normalization: bool, path: str) -> Tuple[Dataset, Dataset, Compose]:
    """
    Loads image data from given path.

    Args:
        img_size (int): Image size
        training_split_ratio (float): Ratio for training data split
        augmentation (bool): Whether data should be augmented
        compute_normalization (bool): Whether normalization parameters should be computed from dataset
        path (str): Root path to dataset

    Returns:
        Tuple[Dataset, Dataset, Compose]: Training dataset, validation dataset and transform
    """
    
    if compute_normalization:
        transform = transforms.Compose([transforms.ToTensor()])
        dataset = datasets.ImageFolder(root=path, transform=transform)
        normalization = transforms.Normalize(*compute_normalization_params(dataset))
    else:
        normalization = NORMALIZE_CUSTOM
    if augmentation:
        transform = transforms.Compose([transforms.Resize((img_size, img_size)),
                                        transforms.ToTensor(), 
                                        transforms.Lambda(lambda x: F.pad(x.unsqueeze(0), 
                                                                          (4,4,4,4), mode='reflect').squeeze()), 
                                        transforms.ToPILImage(), 
                                        transforms.RandomCrop(img_size),
                                        transforms.ToTensor(), 
                                        normalization])
    else:
        transform = transforms.Compose([transforms.Resize((img_size, img_size)), 
                                        transforms.ToTensor(),
                                        normalization])
    
    dataset = datasets.ImageFolder(root=path, transform=transform)

    print("Classes:", dataset.classes)
    print("Class <-> Idx Mapping:", dataset.class_to_idx)

    train_dataset, valid_dataset = split_data(dataset, training_split_ratio)
    
    return train_dataset, valid_dataset, transform
    

def load_cifar(img_size: int, augmentation: bool, path: str = "data/") -> Tuple[Dataset, Dataset, Compose]:
    """
    Loads CIFAR-10 dataset.

    Args:
        img_size (int): Image size
        augmentation (bool): Whether data should be augmented
        path (str): Root path to dataset

    Returns:
        Tuple[Dataset, Dataset]: Training dataset and validation dataset
    """
    
    if augmentation:
        transform = transforms.Compose([transforms.Resize((img_size, img_size)),
                                        transforms.ToTensor(), 
                                        transforms.Lambda(lambda x: F.pad(x.unsqueeze(0), 
                                                                          (4,4,4,4), mode='reflect').squeeze()), 
                                        transforms.ToPILImage(), 
                                        transforms.RandomCrop(img_size), 
                                        transforms.RandomHorizontalFlip(),
                                        transforms.ToTensor(), 
                                        NORMALIZE_CIFAR10])
    else:
        transform = transforms.Compose([transforms.Resize((img_size, img_size)), 
                                        transforms.ToTensor(),
                                        NORMALIZE_CIFAR10])
    
    train_dataset = datasets.CIFAR10(root=path, download=True, transform=transform)
    valid_dataset = datasets.CIFAR10(root=path, train=False, transform=transform)
    
    return train_dataset, valid_dataset, transform


def create_data_loaders(train_dataset: Dataset, valid_dataset: Dataset, 
                        batch_size: int) -> Tuple[DataLoader, DataLoader]:
    """
    Creates data loaders for given training and validation datasets.

    Args:
        train_dataset (Dataset): Training dataset
        valid_dataset (Dataset): Validation dataset
        batch_size (int): Size of mini batches

    Returns:
        Tuple[DataLoader, DataLoader]: Training data loader, validation data loader
    """


    train_loader = DataLoader(dataset=train_dataset, 
                            batch_size=batch_size, 
                            shuffle=True)
    valid_loader = DataLoader(dataset=valid_dataset, 
                            batch_size=batch_size, 
                            shuffle=False)
    
    return train_loader, valid_loader