from typing import Callable, Iterable

import torch
from torch.optim._multi_tensor import SGD
from torch.tensor import Tensor

class SAMSGD(SGD):
    def __init__(self, params: Iterable[Tensor], lr: float, momentum: float = 0, damping: float = 0, 
                 weight_decay: float = 0, nesterov: bool = False, rho: float = 2) -> None:
        """
        Represents Sharp-Aware Minimization optimizer.

        Args:
            params (Iterable[Tensor]): Tensor to be optimized
            lr (float): Lerning rate
            momentum (float, optional): Momentum factor. Defaults to 0.
            damping (float, optional): Damping factor. Defaults to 0.
            weight_decay (float, optional): Weight decay factor. Defaults to 0.
            nesterov (bool, optional): Usage of nesterov momentum. Defaults to False.
            rho (float, optional): SAM neighborhood size. Defaults to 2.

        Raises:
            ValueError: Rho must not be non-positive
            ValueError: Only one parameter group is supported
        """
        
        if rho <= 0:
            raise ValueError(f"Invalid neighborhood size: {rho}")
        
        super().__init__(params, lr, momentum, damping, weight_decay, nesterov)
        
        if len(self.param_groups) > 1:
            raise ValueError("Not supported")
        
        self.param_groups[0]["rho"] = rho
        

    @torch.no_grad()
    def step(self, closure: Callable) -> Tensor:
        """
        Performs a single optimization step.

        Args:
            closure (Callable): A closure that reevaluates the model and returns the loss.

        Returns:
            Tensor: Computed loss
        """
        
        
        closure = torch.enable_grad()(closure)
        loss = closure().detach()

        for group in self.param_groups:
            grads = []
            params_with_grads = []

            rho = group['rho']
            # update internal_optim's learning rate

            for p in group['params']:
                if p.grad is not None:
                    # without clone().detach(), p.grad will be zeroed by closure()
                    grads.append(p.grad.clone().detach())
                    params_with_grads.append(p)
            device = grads[0].device

            # compute \hat{\epsilon}=\rho/\norm{g}\|g\|
            grad_norm = torch.stack([g.detach().norm(2).to(device) for g in grads]).norm(2)
            epsilon = grads  # alias for readability
            torch._foreach_mul_(epsilon, rho / grad_norm)

            # virtual step toward \epsilon
            torch._foreach_add_(params_with_grads, epsilon)
            # compute g=\nabla_w L_B(w)|_{w+\hat{\epsilon}}
            closure()
            # virtual step back to the original point
            torch._foreach_sub_(params_with_grads, epsilon)

        super().step()
        return loss