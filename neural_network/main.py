import argparse

import analyze_nn
import data
import model
import torch
from torch import cuda, nn, optim
import train


parser = argparse.ArgumentParser(description='AI Controlled Vehicle - WideResNet Training')
parser.add_argument('--dataset', default='traffic_signs', type=str,
                    help='dataset (cifar10, traffic_signs [default])')
parser.add_argument('--img-size', default=32, type=int,
                    help='image size (px)')
parser.add_argument('--epochs', default=200, type=int,
                    help='number of total epochs to run')
parser.add_argument('--start-epoch', default=0, type=int,
                    help='manual epoch number (useful on restarts)')
parser.add_argument('--batch-size', default=32, type=int,
                    help='mini-batch size (default: 32)')
parser.add_argument('--learning-rate', default=0.1, type=float,
                    help='initial learning rate')
parser.add_argument('--early-stopping-epochs', default=-1, type=int, help='early stopping of training, if number of epochs without improvement is exceeded')
parser.add_argument('--beta1', default=0.9, type=float,
                    help='beta1 parameter for AdamW optimizer (default: 0.9)')
parser.add_argument('--beta2', default=0.999, type=float,
                    help='beta2 parameter for AdamW optimizer (default: 0.999)')
parser.add_argument('--weight-decay-coeff', default=1e-2, type=float,
                    help='weight decay coefficient for AdamW optimizer (default: 1e-2)')
parser.add_argument('--training-split-ratio', default=0.8, type=float,
                    help='ratio for training data split')
parser.add_argument('--print-every', default=1, type=int,
                    help='print frequency (default: 1)')
parser.add_argument('--layers', default=10, type=int,
                    help='total number of layers (default: 10)')
parser.add_argument('--widen-factor', default=4, type=int,
                    help='widen factor (default: 4)')
parser.add_argument('--droprate', default=0.0, type=float,
                    help='dropout probability (default: 0.0)')
parser.add_argument('--no-augment', dest='augment', action='store_false',
                    help='whether to use standard augmentation (default: True)')
parser.add_argument('--no-cuda', dest='cuda', action='store_false',
                    help='whether to use CUDA, if available (default: True)')
parser.add_argument('--no-nn-analyzing', dest='nn-analyzing', action='store_false',
                    help='whether to analyze neural network architecture (default: False)')
parser.add_argument('--no-normalization-computation', dest='normalization-computation', action='store_false',
                    help='whether to compute normalization parameters (default: False)')
parser.set_defaults(augment=True)
parser.set_defaults(cuda=True)
parser.set_defaults(nn_analyzing=True)
parser.set_defaults(normalization_computation=False)


def main():
    args = parser.parse_args()
    
    assert args.epochs > args.start_epoch - 1
    
    device = 'cuda:0' if cuda.is_available() and args.cuda else 'cpu'
    
    if args.dataset == "cifar10":
        train_dataset, valid_dataset, transform = data.load_cifar(args.img_size, args.augment)
        num_classes = 10
    elif args.dataset == "traffic_signs":
        path = "C:\\Users\\denni\\Desktop\\Training_data_set"
        train_dataset, valid_dataset, transform = data.load_img_data(args.img_size, args.training_split_ratio, args.augment, args.normalization_computation, path)
        num_classes = 6
    else:
        raise ValueError("Unknown Dataset: %s" % args.dataset)
    
    train_loader, valid_loader = data.create_data_loaders(train_dataset, valid_dataset, args.batch_size)
    
    wrn = model.WideResNet(num_classes, args.layers, args.widen_factor, args.droprate).to(device)
    if args.nn_analyzing:
        analyze_nn.create_all_figures([wrn])
    loss_func = nn.CrossEntropyLoss().to(device)
    optimizer = optim.AdamW(wrn.parameters(), args.learning_rate, (args.beta1, args.beta2), 
                                 weight_decay=args.weight_decay_coeff)
    lr_scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=len(train_loader)*args.epochs)
    
    best_model, optimizer, (train_loss_history, valid_loss_history), (train_acc_history, valid_acc_history) = train.training_loop(wrn, loss_func, optimizer, lr_scheduler,
                                                                                                              train_loader, valid_loader, args.epochs, args.start_epoch, 
                                                                                                              args.early_stopping_epochs, device, args.print_every)
    
    torch.save(best_model, 'wrn.pth')

    
if __name__ == "__main__":
    main()