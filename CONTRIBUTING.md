# Coding guidelines
## Naming conventions
- use names that can be understood without any documentation (e.g. `server_ip` instead of `s_ip`)
- in general all names of variables, functions, methods, ... should be in lower case  with `_` as seperator (e.g. `function_add`)
- class names should always be in upper case without any seperator (e.g. `ExampleClass`)

## General conventions for scripts
- scripts should contain `#!/usr/bin/env python3` as first line
- afterwards an empty line and in the next line a small script definition (docstring, e.g. `"""Script implements camera stream"""`)should be added
- finally the imports follow, seperated from the docstring by another empty line

## Definition of classes
- after the definition `class ClassName(object):` the next line should contain a simple description what the class is used for
```
class TCPServer(object):
    """Represents the TCP server and can be started with ..."""
```
- every method should include a docstring, which is a short description of its functionality, the input parameter and the return value(-s)
```
    def start_server(ip_address, port):
        """Start the tcp server on the given port.
        
        :parameter
        ip_address  --  ip address to start TCP server on
        port        --  port number the TCP server uses
        
        :return
        state   --  status on start (boolean)
        """
```

