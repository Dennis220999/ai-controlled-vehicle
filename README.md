# Development of an AI-controlled vehicle

A self-driving car, also known as an autonomous vehicle (AV) or automated vehicle and fully automated vehicle (in the European Union) is a vehicle that is capable of sensing its environment and moving safely with little or no human input. 
These machines combine a variety of sensors to collect data of their surroundings and interpret the stream of data to identify appropriate navigation paths.

First, a suitable platform for AI-controlled driving will be identified. Existing construction kits will be compared. The team is going to gather more information about AI-controlled driving. 
Up next is the assembling of individual components (like SoC, camera, transmitter) and the development of a control concept.
Finally, the car should be able to cross an obstacle course.